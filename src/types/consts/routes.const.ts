export const ROUTES = {
  home: "/",
  addProject: "project/add",
  showProject: "project/:projectId",
};
