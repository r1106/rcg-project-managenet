import { IProject } from "../interfaces/project.interface";

export const INIT_PROJECTS: IProject[] = [
  {
    id: "",
    title: "",
    desc: "",
    date: "",
    tasks: [],
  },
];
