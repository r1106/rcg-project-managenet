import { ITask } from "./task.interface";

export interface IProject {
  id: string;
  title: string;
  desc: string;
  date: string;
  tasks: ITask[];
}
