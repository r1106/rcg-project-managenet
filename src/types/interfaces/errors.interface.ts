export interface IErrors {
  title?: string;
  desc?: string;
  date?: string;
}
