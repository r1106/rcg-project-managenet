import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import App, { productsLoader } from "./App";
import AddProject from "./components/add-project/AddProject";
import ErrorPage from "./components/error-page/ErrorPage";
import Project, { getProjectLoader } from "./components/project/Project";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { ROUTES } from "./types/consts/routes.const";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
const router = createBrowserRouter([
  {
    path: ROUTES.home,
    element: <App></App>,
    errorElement: <ErrorPage></ErrorPage>,
    loader: productsLoader,
    children: [
      {
        path: ROUTES.showProject,
        element: <Project></Project>,
        loader: getProjectLoader,
      },
      {
        path: ROUTES.addProject,
        element: <AddProject></AddProject>,
      },
    ],
  },
]);

root.render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
