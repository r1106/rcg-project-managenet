import { Outlet, useLoaderData, useLocation } from "react-router-dom";
import NoProjectSelected from "./components/no-project-selected/NoProjectSelected";
import Sidebar from "./components/sidebar/Sidebar";
import { getProjects } from "./services/products.service";
import { ROUTES } from "./types/consts/routes.const";
import { IProject } from "./types/interfaces/project.interface";

export async function productsLoader() {
  const projects = await getProjects();
  return projects;
}

function App(): JSX.Element {
  const projects: IProject[] = useLoaderData() as IProject[];
  const location = useLocation();

  return (
    <>
      <Sidebar projects={projects}></Sidebar>
      <div className="mt-16 p-4 sm:ml-64">
        {location.pathname === ROUTES.home && (
          <NoProjectSelected></NoProjectSelected>
        )}
        <Outlet></Outlet>
      </div>
    </>
  );
}

export default App;
