import { Link } from "react-router-dom";
import { ROUTES } from "../../types/consts/routes.const";
import { IProject } from "../../types/interfaces/project.interface";

function Sidebar({ projects }: { projects: IProject[] }): JSX.Element {
  return (
    <aside
      id="default-sidebar"
      className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
      aria-label="Sidebar"
    >
      <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
        <div className="flex justify-between items-baseline">
          <Link to={ROUTES.home}>
            <h1 className="uppercase text-white font-bold pb-8">
              Your Projects
            </h1>
          </Link>
          <Link
            to={ROUTES.addProject}
            className="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow"
          >
            +
          </Link>
        </div>
        {projects.length ? (
          <ul className="space-y-2 font-medium">
            {projects.map((project) => {
              return (
                <li
                  key={project.id}
                  className="bg-gray-600 px-4 py-4 text-white"
                >
                  <Link to={`/project/${project.id}`}>{project.title}</Link>
                </li>
              );
            })}
          </ul>
        ) : null}
      </div>
    </aside>
  );
}

export default Sidebar;
