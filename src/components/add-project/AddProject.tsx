import { Dispatch, SetStateAction, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { createProject } from "../../services/products.service";
import { INIT_PROJECTS } from "../../types/consts/init-projects.const";
import { ROUTES } from "../../types/consts/routes.const";
import { IErrors } from "../../types/interfaces/errors.interface";
import { IProject } from "../../types/interfaces/project.interface";

function AddProject(): JSX.Element {
  const navigate = useNavigate();
  const [errors, setErrors]: [IErrors, Dispatch<SetStateAction<IErrors>>] =
    useState({});
  const [formData, setFormData] = useState<IProject>(INIT_PROJECTS[0]);

  const validationValues = (formData: IProject) => {
    let errors: IErrors = {};
    if (formData.title.length <= 0) {
      errors.title = "No Title set";
    }
    if (formData.desc.length <= 0) {
      errors.desc = "No Desc set";
    }
    if (formData.date.length <= 0) {
      errors.date = "No Date set";
    }

    return errors;
  };

  function handleInputChange(
    event:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
  ) {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  }

  function onSubmit(event: React.FormEvent): void {
    event.preventDefault();
    setErrors((_errors) => {
      const foundErrors = validationValues(formData);

      if (Object.keys(foundErrors).length === 0) {
        createProject(formData);
        navigate(0);
      }

      return foundErrors;
    });
  }

  return (
    <div className="flex justify-center">
      <form onSubmit={(event) => onSubmit(event)}>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="title"
          >
            Title
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            name="title"
            id="title"
            type="text"
            placeholder="Please Enter a Title"
            value={formData.title}
            onChange={(event) => handleInputChange(event)}
          />
          {errors.title && (
            <p className="text-red-500 text-xs italic">{errors.title}</p>
          )}
        </div>
        <div className="mb-6">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="desc"
          >
            Description
          </label>
          <textarea
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline resize-none h-[200px]"
            id="desc"
            name="desc"
            placeholder="Please enter a Description"
            value={formData.desc}
            onChange={(event) => handleInputChange(event)}
          ></textarea>
          {errors.desc && (
            <p className="text-red-500 text-xs italic">{errors.desc}</p>
          )}
        </div>
        <div className="flex items-center justify-between"></div>
        <div className="mb-6">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="desc"
          >
            Due Date
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="date"
            name="date"
            type="date"
            placeholder="dd.mm.yyy"
            value={formData.date}
            onChange={(event) => handleInputChange(event)}
          />
          {errors.date && (
            <p className="text-red-500 text-xs italic">{errors.date}</p>
          )}
        </div>
        <div className="border-t-4 border-black py-8 mt-4 buttons flex items-center justify-between">
          <Link
            className="bg-red-950 hover:bg-red-700 text-white font-semibold p-4 border border-red-400 rounded shadow"
            to={ROUTES.home}
          >
            Cancel
          </Link>
          <button
            className="bg-black hover:bg-gray-900 text-white font-semibold py-4 px-4 border border-gray-400 rounded shadow"
            type="submit"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );
}

export default AddProject;
