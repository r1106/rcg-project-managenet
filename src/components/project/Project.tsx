import { useState } from "react";
import { useLoaderData, useNavigate } from "react-router-dom";
import {
  createRandonId,
  deleteProject,
  getProject,
  updateProject,
} from "../../services/products.service";
import { ROUTES } from "../../types/consts/routes.const";
import { IProject } from "../../types/interfaces/project.interface";
import { ITask } from "../../types/interfaces/task.interface";

export async function getProjectLoader({
  params,
}: any): Promise<IProject | null | undefined> {
  const project = await getProject(params.projectId);

  return project;
}

function Project(): JSX.Element {
  const project: IProject = useLoaderData() as IProject;
  const navigation = useNavigate();
  const [taskData, setTaskData] = useState<ITask>({ id: "", title: "" });

  function handleDelete(id: string) {
    deleteProject(id);
    navigation(ROUTES.home);
    reloadPage();
  }

  function handleTaskInputChange(
    event: React.ChangeEvent<HTMLInputElement>
  ): void {
    const { value } = event.target;
    const newTask = { id: createRandonId(), title: value };
    setTaskData(() => newTask);
  }

  function handleAddTask(id: string): void {
    const projectUpdate: IProject = {
      ...project,
    };
    projectUpdate.tasks.push(taskData);

    updateProject(id, projectUpdate);
    reloadPage();
  }

  function handleTaskDelete(taskId: string): void {
    const index = project.tasks.findIndex((task) => task.id === taskId);

    if (index > -1) {
      project.tasks.splice(index, 1);
      updateProject(project.id, project);
      reloadPage();
    }
  }

  function reloadPage(): void {
    navigation(0);
  }

  return (
    <div className="flex flex-col items-center justify-center">
      <div className="border-b-4 border-black w-[900px] min-h-[400px] p-4">
        <div className="flex items-center justify-between mb-8">
          <span className="text-6xl">{project.title}</span>
          <button
            className="bg-red-950 hover:bg-red-700 text-white font-semibold py-2 px-2 border border-red-400 rounded shadow ml-8"
            onClick={() => handleDelete(project.id)}
          >
            Delete Project
          </button>
        </div>
        <p className="text-3xl mb-8">{project.date}</p>
        <pre className="text-2xl">{project.desc}</pre>
      </div>

      <div className="w-[900px] h-[400px] p-4">
        <h1 className="text-4xl mb-8">Tasks</h1>
        <div className="flex items-center justify-between">
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="title"
            >
              Add a new Task
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              name="title"
              id="title"
              type="text"
              placeholder="Please Enter a Task"
              onChange={(event) => handleTaskInputChange(event)}
            />
          </div>
          <button
            className="bg-black hover:bg-gray-900 text-white font-semibold py-4 px-4 border border-gray-400 rounded shadow ml-8"
            onClick={(_event) => handleAddTask(project.id)}
          >
            Add Task
          </button>
        </div>
        <div>
          <input type="text" />
        </div>
        {project.tasks.length === 0 && (
          <p>This Project does not have any tasks yet.</p>
        )}
        {project.tasks.length > 0 &&
          project.tasks.map((task, index) => {
            return (
              <div key={task.id} className="flex mb-4">
                <span className="flex-1">{index + 1}</span>
                <p className="flex-[20]">{task.title}</p>
                <button
                  className="flex-1 bg-red-950 hover:bg-red-700 text-white font-semibold py-2 px-2 border border-red-400 rounded shadow ml-8"
                  onClick={() => handleTaskDelete(task.id)}
                >
                  Delete
                </button>
              </div>
            );
          })}
      </div>
    </div>
  );
}

export default Project;
