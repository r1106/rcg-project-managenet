import { Link } from "react-router-dom";
import logo from "../../assets/no-projects.png";
import { ROUTES } from "../../types/consts/routes.const";

function NoProjectSelected(): JSX.Element {
  return (
    <div className="flex flex-col items-center justify-center">
      <img className="w-[100px]" src={logo} alt="Logo" />
      <h1 className="text-3xl text-gray-700 font-bold px-8 pt-8 pb-2">
        No Project Selected
      </h1>
      <p className="text-1xl text-gray-600 font-bold px-4 py-4">
        Select a project or get started with a new one
      </p>
      <Link
        to={ROUTES.addProject}
        className="bg-black hover:bg-gray-900 text-white font-semibold py-4 px-4 border border-gray-400 rounded shadow"
      >
        Create a New Project
      </Link>
    </div>
  );
}

export default NoProjectSelected;
