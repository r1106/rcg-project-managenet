import { format } from "date-fns";
import localforage from "localforage";
import { IProject } from "../types/interfaces/project.interface";

export async function getProjects(query?: string): Promise<IProject[]> {
  const projects: IProject[] | null = await localforage.getItem("projects");

  return projects ?? [];
}

export async function createProject(formData: IProject): Promise<IProject> {
  const id: string = createRandonId();
  const date = new Date(formData.date);
  const formattedDate = format(date, "dd.MM.yyyy");
  const project: IProject = {
    id,
    title: formData.title,
    desc: formData.desc,
    date: formattedDate,
    tasks: [],
  };
  const projects: IProject[] | [] = await getProjects();

  projects.unshift(project);
  await setProjects(projects);
  return project;
}

export async function getProject(
  id: string
): Promise<IProject | null | undefined> {
  const projects: IProject[] | null = await getProjects();

  return projects ? projects.find((project) => project.id === id) : null;
}

export async function updateProject(
  id: string,
  updates: IProject
): Promise<IProject> {
  const projects: IProject[] | null = await getProjects();
  const project = projects.find((project) => project.id === id);

  if (!project) throw new Error("No Project found for", id as ErrorOptions);
  Object.assign(project, updates);
  await setProjects(projects);
  return project;
}

export async function deleteProject(id: string): Promise<boolean> {
  const projects: IProject[] | null = await getProjects();
  const index = projects.findIndex((project) => project.id === id);

  if (index > -1) {
    projects.splice(index, 1);
    await setProjects(projects);
    return true;
  }
  return false;
}

function setProjects(projects: IProject[]): Promise<IProject[]> {
  return localforage.setItem("projects", projects);
}

export function createRandonId(): string {
  return Math.random().toString(36).substring(2, 9);
}
